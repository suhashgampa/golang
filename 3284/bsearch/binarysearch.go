// binary search on array to find the element
package main

import "fmt"

func binarySearch(key int, array []int) int {
	low := 0
	high := len(array) - 1
	for low <= high {
		mid := low+(high-low) / 2
		if array[mid] < key {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	if low == len(array) || array[low] != key {
		return -1
	}
	return low
}

func main() {
	items := []int{1, 2, 9, 18, 31, 55, 63, 70, 100}
	fmt.Println(binarySearch(63, items))
}
