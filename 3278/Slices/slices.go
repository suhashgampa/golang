//This program shows how slices in go are implemented

package main

import "fmt"

func main1() {
	array1 := [5]int{4, 5, 6}
	var array2 = [5]int{4, 5, 6}
	if array1 == array2 {
		fmt.Println("equal")
	} else {
		fmt.Println("not equal")
	}
	fmt.Println(array1)

	for i := 0; i < len(array1); i++ {
		fmt.Printf("%d ", array1[i])
	}
	fmt.Println()
}
