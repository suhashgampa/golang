Golang codes written by interns during **Searce Internship Training 2023**

Programs are grouped by employee id directories.

# Exploratory programs:
1. Hello world
2. Array
3. Map
4. Slice
5. Goroutine
6. Waitgroup

# Assigments:
1. Calculate factorial of a number
2. Implement binary search in Go
