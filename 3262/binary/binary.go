package main

import (
	"fmt"
	"sort"
)

func binarySearch(arr []int, size int, toFind int) bool {

	start := 0
	end := size - 1
	mid := (end-start)/2 + start

	for start <= end {
		if arr[mid] == toFind {
			return true
		} else if arr[mid] > toFind {
			start = mid + 1
		} else {
			end = mid - 1
		}
	}
	return false
}

func main() {
	fmt.Printf("Enter size of your array: ")
	var size int
	fmt.Scanln(&size)
	var arr = make([]int, size)
	for i := 0; i < size; i++ {
		fmt.Printf("Enter %dth element: ", i)
		fmt.Scanf("%d", &arr[i])
	}
	sort.Ints(arr)
	fmt.Print("Enter the number you want to search in array: ")
	var toFind int
	fmt.Scanln(&toFind)
	numFound := binarySearch(arr, size, toFind)
	if numFound == true {
		fmt.Printf("The number %d is present in the array\n", toFind)
	} else {
		fmt.Printf("The number %d was not found in the array\n", toFind)
	}
}
