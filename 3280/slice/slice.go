// demonstrating use of slice(append) and adding all elements of a slice

package main

import "fmt"

func slicing() {
	array1 := [5]int{4, 5, 7}
	slice1 := []int{1, 2}

	fmt.Println(array1)
	fmt.Println(slice1)

	slice1 = append(slice1, 6)
	fmt.Println(slice1)
	fmt.Println(slice1[1:])
	fmt.Println(slice1[1:2])
}

func add_slice(slice []int) int {
	sum := 0
	for i := 0; i < len(slice); i++ {
		sum += slice[i]
	}
	return sum
}

func main() {
	slicing()
	array3 := []int{1, 2, 3, 4, 5, 6, 7}
	fmt.Println(add_slice(array3))
}
