// This program implements the binary search operation on an array.
package main

import "fmt"

func search(nums []int, target int) int {
    var i, j = 0, len(nums) - 1
    for i <= j {
        var mid = i + (j-i)/2
        if nums[mid] == target {
            return mid
        }
        if target > nums[mid] {
            i = mid + 1
        } else {
            j = mid - 1
        }
    }
    return -1
}

func main() {
    var fix = []int{9, 7, 6, 4, 5, 3, 2, 4}
    var ans = search(fix, 4)
    fmt.Print(ans)
}
