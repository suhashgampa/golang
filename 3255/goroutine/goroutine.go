/// goroutine problem which add numbers and return

package main

func sum(slice2 []int) int {
	sum1 := 0
	for i := 0; i < len(slice2); i++ {
		sum1 = sum1 + slice2[i]
	}
	return sum1
}
